class BlogconfigsController < ApplicationController
	layout 'blogconfigs'
	before_action :authenticate_account!, only: [:login_check, :edit, :index]

	def inde
		@msg = 'this is sample page.'
	end


	def login_check
		@account = current_account
		@msg = 'you logined ' 
	end


	def index
		@blogconfig = Blogconfig.find 1
	end


	def edit
		@blogconfig = Blogconfig.find 1
		if request.patch? then
			@blogconfig.update blogconfig_params
			redirect_to '/blogconfigs/index'
		end
	end


	private
	def blogconfig_params
		params.require(:blogconfig).permit(:title, :subtitle, :stylename)
	end


end