class AddSongToBlogposts < ActiveRecord::Migration[5.1]
  def change
    add_column :blogposts, :songtitle, :text
    add_column :blogposts, :songurl, :text
  end
end
