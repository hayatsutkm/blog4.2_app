Rails.application.routes.draw do
  devise_for :accounts
  as :account do
    get 'accounts/edit' => 'devise/registrations#edit', :as => 'edit_account_registration'
    put 'accounts' => 'devise/registrations#update', :as => 'account_registration'
  end
  get 'accounts/sign_out', to: 'blogs#index'
  get 'blogconfigs/inde', to: 'blogconfigs#inde'
  get 'bloggenres/inde', to: 'bloggenres#inde'
  get 'blogposts/inde', to: 'blogposts#inde'
  get 'blogconfigs/login_check'
  get 'bloggenres/login_check'
  get 'blogposts/login_check'

  get '/', to: 'blogs#index'
  get 'blogs/index'
  get 'blogs', to: 'blogs#index'
  get 'blogs/:page', to: 'blogs#index'

  get 'blogs/genre/:id', to: 'blogs#genre'
  get 'blogs/genre/:id/:page', to: 'blogs#genre'

  get 'blogs/show/:id', to: 'blogs#show'

  get 'blogposts/index'
  get 'blogposts', to: 'blogposts#index'

  get 'blogposts/delete/:id', to: 'blogposts#delete'
  post 'blogposts/delete', to: 'blogposts#delete'
  post 'blogposts/delete/:id', to: 'blogposts#delete'

  get 'blogposts/add'
  post 'blogposts/add'

  get 'blogposts/:id', to: 'blogposts#edit'
  patch 'blogposts/:id', to: 'blogposts#edit'

  get 'blogposts/delete'

  get 'bloggenres/index'
  get 'bloggenres', to: 'bloggenres#index'

  get 'bloggenres/add'
  post 'bloggenres/add'

  get 'bloggenres/:id', to: 'bloggenres#edit'
  patch 'bloggenres/:id', to: 'bloggenres#edit'

  get 'blogconfigs/index'
  get 'blogconfig/:id', to: 'blogconfigs#index'

  get 'blogconfigs/edit'
  patch 'blogconfigs/edit'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
